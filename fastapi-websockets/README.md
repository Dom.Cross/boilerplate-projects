# **Getting set up**

Create a directory for this playful project to live in. **Do not** put this in an existing project. Create its own directory somewhere.

It may have been a while since you last created a virtual environment, so here are those steps, again:

1. Create the virtual environment

    ```bash
    python -m venv .venv

    ```

2. Activate the virtual environment the way you do on your operating system
3. Upgrade **pip** in your virtual environment

    ```bash
    python -m pip install --upgrade pip

    ```

4. Install the dependencies with the following command:

    ```bash
    pip install 'fastapi[all]' websockets
    ```

    # **Test it out**

    Sure, you type something in the input field, click the *Send* button, and it shows up in a list. You've done similar stuff with normal JavaScript.

    In your terminal, you should now be able to start up this simple application using the following command:

    ```bash
    uvicorn main:app --reload
    ```

    To **really** test this, you need to open **two** browser windows, each pointing at http://localhost:8000/.

    Once you have two windows open, try sending a message in one of them. It shows up in both!

    # **Using this in your project**

    If you want to use something like this in your project, you'll want to do something like the following:

    1. Assuming you want to add this to an existing FastAPI project, go to the `[websockets` project](https://pypi.org/project/websockets/)  and find the latest version
    2. Add an entry to your *requirements.txt* file with that version from the previous step
    3. Restart your Docker services to have it install that new library into your image/container
    4. Create a WebSocket connection in the mounting of one of your components
    5. When new messages arrive, add them to some state in your React application
    6. Copy, paste, and extend the FastAPI code to get your backend handling requests (you may want to create a `ConnectionManager` class like in the [FastAPI documentation](https://fastapi.tiangolo.com/advanced/websockets/#handling-disconnections-and-multiple-clients)  )
    7. Profit!
